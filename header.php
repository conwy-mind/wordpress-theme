<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!-- Designed and Coded by https://michael.markie.co  -->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="theme-color" content="#003377">
	<meta name="msapplication-TileColor" content="#003377">

    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/32-icon.png">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/68-icon.png">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/76-icon.png" sizes="76x76">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/120-icon.png" sizes="120x120">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icons/152-icon.png" sizes="152x152">
	
    <meta content="<?php echo get_template_directory_uri(); ?>/assets/images/icons/64-icon.png" property="og:image">
    <meta content="64" property="og:image:width">
    <meta content="64" property="og:image:height">


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="urgent">
		<div class="wrap">

			<?php if ( has_nav_menu( 'urgent' ) ) : ?>

				<nav class="navigation-quick" role="navigation">
					<?php wp_nav_menu( array(
						'container'      => false,
						'menu_id'        => '',
						'menu_class'     => '',
						'theme_location' => 'urgent',
						'depth'			 => 1,
					) ); ?>
				</nav><!-- .navigation-quick -->

			<?php endif; ?>

		</div><!-- .row-flex -->
	</header>
	<header class="primary">
		<div class="wrap">
			<div class="logo">
				<a href="/" class="icon icon-mind-cy"></a>
				<a href="/" class="icon icon-local"></a>
			</div><!-- .logo -->

			<?php if ( is_active_sidebar( 'header-widget' ) ) : ?>
				<aside class="widget-area" role="complementary">

					<?php if ( is_active_sidebar( 'header-widget' ) ) : ?>

						<div class="header-widget"><?php dynamic_sidebar( 'header-widget' ); ?></div>

					<?php endif; ?>
				</aside>
			<?php endif; ?>
		</div><!-- .wrap -->

		<div class="wrap">
			<?php if ( has_nav_menu( 'top' ) ) : ?>
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<label for="show_menu" class="icon icon-hamburger show_menu"></label>
					<input type="checkbox" id="show_menu">

					<?php wp_nav_menu(array('container' => false, 'menu_id' => '', 'menu_class' => '', 'theme_location' => 'top',)); ?>

				</nav><!-- #site-navigation -->
			<?php endif; ?>

		</div><!-- .wrap -->
	</header><!-- .primary -->

	<?php if ( has_post_thumbnail() ) : ?>

		<div class="feature">
			<?php the_post_thumbnail(); ?>	
			<canvas width="1" height="1"></canvas>
		</div><!-- .feature -->

	<?php endif; ?>

	<div class="wrap">
