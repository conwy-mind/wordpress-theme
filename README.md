# Conwy Mind


### Branding

Branding is all centred around local Mind.

### Compiling

Node.js, NPM & Gulp is required for building the website.

Majority of assets are built using Gulp (SASS and images)

```bash
npm install
```

After which you can build using 3 methods depending on your needs.

If you want to watch for changes in SASS

```bash
gulp watcherSass
```

Alternatively you can build the SASS on its own

```bash
gulp sass
```

Converting images to webp

```bash
gulp webp
```

Build SASS and webp images

```bash
npm run build
```
