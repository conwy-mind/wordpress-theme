<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!-- Designed and Coded by https://michael.markie.co  -->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <a href="/">
        <div class="icon icon-mind-cy"></div>
        <div class="icon icon-local"></div>
    </a>

    <h1>My apologies!</h1>
    <p>The page you have requested cannot be found.</p>

    <p>Feel free to browse our website by <a href="/" class="underline">clicking here</a> to see what else we can offer.</p>
</body>
</html>