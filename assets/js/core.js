window.addEventListener('load', function () {

    // var height = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight);
    // var width = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth);

    // if (height > 700 && width > 1024) {
    //     setInterval(function () {
    //         var height = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight);
    //         var width = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth);

    //         if (height > 700 && width > 1024) {
    //             var 
    //                 scroll = this.scrollY,
    //                 primary = document.querySelector('header.primary'),
    //                 urgent = document.querySelector('header.urgent'),
    //                 body = document.querySelector('body');

    //             if (scroll > 49) {

    //                 var header_height = primary.clientHeight+'px';
    //                 urgent.classList.add('hide');
    //                 primary.classList.add('fixed');
    //                 body.style.paddingTop = header_height;

    //             } else {

    //                 urgent.classList.remove('hide');
    //                 primary.classList.remove('fixed');
    //                 body.style.paddingTop = 0;

    //             }
    //         }

    //     }, 50);
    // }

    var feature = document.querySelector('.feature');
    if (feature) {
        var img = new Image();
        img.src = feature.querySelector('img').src;

        img.addEventListener('load', function () {

            var height = img.naturalHeight;

            var canvas = feature.querySelector('canvas');
            var context = canvas.getContext('2d');

            canvas.height = height;

            context.drawImage(img, 0, 0);

            var data = context.getImageData(0, 0, 1, 1).data;
            var bottom_data = context.getImageData(0, ( height - 1 ), 1, ( height )).data;
            
            var start = `rgb(${data[0]},${data[1]},${data[2]})`;
            var end = `rgb(${bottom_data[0]},${bottom_data[1]},${bottom_data[2]})`;

            feature.style.background = `linear-gradient(180deg, ${start} 0%, ${end} 100%)`;

            canvas.parentNode.removeChild(canvas);

        });
    }

});
