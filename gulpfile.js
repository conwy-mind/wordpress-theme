const { src, dest, series, parallel, watch } = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');

const autocrop = require("gulp-autocrop");
const rename = require("gulp-rename");

const imagemin = require("gulp-imagemin");
const webp = require("imagemin-webp");
const extReplace = require("gulp-ext-replace");


function sassTask(cb) {
	
    src('assets/sass/core.sass')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(dest('assets/css'))
		.on('finish', cb);
}
function watchSassTask(cb) {

	watch('assets/sass/*.sass', function(cb) {

	    src('assets/sass/core.sass')
	    	.pipe(sourcemaps.init())
	        .pipe(sass().on('error', sass.logError))
	        .pipe(sourcemaps.write())
	        .pipe(dest('assets/css'))
			.on('finish', cb);
	});

}


function webpTask(cb) {


	src('assets/images/*.{png,jpg,jpeg}')
		.pipe(imagemin([
			webp({
				quality: 90
			})
		]))
		.pipe(extReplace('.webp'))
		.pipe(dest('assets/images'))
		.on('finish', cb);
}

function iconTask(cb) {

	const sizes = [32, 64, 68, 76, 120, 128, 152];
	let count = 0;

	sizes.forEach(function (size) {

		src('assets/images/icon.{png,jpg,jpeg}')
			.pipe(autocrop({
				height: size,
				width: size
			}))
			.pipe(rename({
				prefix: size+'-'
			}))
			.pipe(imagemin())
			.pipe(dest('assets/images/icons'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			})


	});
}

exports.sass = sassTask;
exports.webp = webpTask;
exports.icon = iconTask;
exports.watchSass = watchSassTask;

exports.default = series(sassTask, webpTask);