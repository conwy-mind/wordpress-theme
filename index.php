<?php get_header(); ?>

	<div id="primary" class="content-area">

		<?php if ( is_home() && ! is_front_page() ) : ?>

			<h2><?php single_post_title(); ?></h2>

		<?php endif; ?>


		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'partials/post-content' );

			endwhile;

			the_posts_pagination( array(
				'prev_text' => '<span class="screen-reader-text">' . __( 'Previous page' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page' ) . '</span>',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page' ) . ' </span>',
			) );

			else :

			get_template_part( 'partials/post-content' );

		endif;
		?>
	</div><!-- #primary -->
<?php //get_sidebar(); ?>

<?php get_footer();
