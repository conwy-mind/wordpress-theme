<?php get_header(); ?>

<div id="primary" class="content-area">
	<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'partials/page-content' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile;
	?>
</div><!-- #primary -->

<?php get_footer();
