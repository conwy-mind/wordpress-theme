	</div><!-- .wrap -->

	<div class="clear"></div>

	<?php if ( has_nav_menu( 'pre-footer' ) ) : ?>
		<nav class="pre-footer" role="navigation">
			<?php
			wp_nav_menu( array(
				'container'      => false,
				'menu_id'        => '',
				'menu_class'     => '',
				'theme_location' => 'pre-footer',
				'depth'          => 1,
			) );
			?>
		</nav><!-- .pre-footer -->
	<?php endif; ?>

	<footer class="site-footer" role="contentinfo">
		<div class="wrap">

			<?php if ( is_active_sidebar( 'sidebar-2' ) || is_active_sidebar( 'sidebar-3' ) ) : ?>
				<aside class="widget-area" role="complementary">

					<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>

						<div class="widget-column-2 footer-widget-1">

							<?php dynamic_sidebar( 'sidebar-2' ); ?>

						</div><!-- .widget-column-2 -->

					<?php endif; ?>

					<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>

						<div class="widget-column-2 footer-widget-2">

							<?php dynamic_sidebar( 'sidebar-3' ); ?>

						</div><!-- .widget-column-2 -->

					<?php endif; ?>

				</aside><!-- .widget-area -->
			<?php endif; ?>

		</div><!-- .wrap -->

		<?php if ( is_active_sidebar( 'full-footer' ) ) : ?>

			<div class="wrap">
				<div class="widget-column full-footer-widget">

					<?php dynamic_sidebar( 'full-footer' ); ?>

				</div><!-- .widget-column -->
			</div><!-- .wrap -->

		<?php endif; ?>
	</footer><!-- .site-footer -->


<?php wp_footer(); ?>
</body>
</html>
